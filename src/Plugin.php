<?php

namespace Smtm\PluginComposerComponentInstaller;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Installer\PackageEvent;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

/**
 * Class Plugin
 *
 * @package Smtm\PluginComposerComponentInstaller
 */
class Plugin implements PluginInterface, EventSubscriberInterface
{
    const EXTRA_KEY = 'savemetenminutes';

    /**
     * @var string
     */
    protected $projectRoot;
    /**
     * @var Composer
     */
    protected $composer;
    /**
     * @var IOInterface
     */
    protected $io;

    /**
     * Constructor
     *
     * Optionally accept the project root into which to install.
     *
     * @param string $projectRoot
     */
    public function __construct($projectRoot = '')
    {
        if (is_string($projectRoot) && !empty($projectRoot) && is_dir($projectRoot)) {
            $this->projectRoot = $projectRoot;
        }
    }

    /**
     * Return list of event handlers in this class.
     *
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'post-package-install'  => 'onPostPackageInstall',
            'pre-package-uninstall' => 'onPrePackageUninstall',
        ];
    }

    /**
     * @param Composer $composer
     * @param IOInterface $io
     * @return void
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $this->io       = $io;
    }

    /**
     * @param PackageEvent $event
     * @return void
     */
    public function onPostPackageInstall(PackageEvent $event)
    {
        $package = $event->getOperation()->getPackage();
        $name    = $package->getName();
        $extra   = $this->getExtraMetadata($package->getExtra());

        if (empty($extra)) {
            // Package does not define anything of interest; do nothing.
            return;
        }

        $prependBackslashCallback           = [
            $this,
            'prependBackslash'
        ];
        $configFile                         = ($this->projectRoot ? $this->projectRoot . '/' : '') . 'config/config.php';
        $config                             = is_file($configFile) ? include $configFile : [];
        $config                             = $config ?: [];
        $config                             =
            array_map(
                $prependBackslashCallback,
                $config
            );
        $configItems                        =
            array_map(
                $prependBackslashCallback,
                $extra['config-provider']
            );
        $configItems                        =
            array_unique(
                array_merge(
                    $config,
                    array_filter(
                        $configItems
                    )
                )
            );
        $createClassConstantLiteralCallback = [
            $this,
            'createClassConstantLiteral'
        ];
        $config                             =
            array_map(
                $createClassConstantLiteralCallback,
                $configItems
            );

        //$configVar = var_export($config, true);
        $configItems = [];
        foreach ($config as $key => $configProvider) {
            $configItems[] = $key . ' => ' . $configProvider . ',';
        }
        $configItems = implode("\n", $configItems);
        $this->io->write("<info>    Added the config provider(s):</info>");
        $this->io->write("<info>    " . implode(', ', $extra['config-provider']) . "</info>");
        $this->io->write("<info>    of " . $name . "</info>");
        file_put_contents(
            $configFile,
            <<< EOT
<?php
return
[
$configItems
]
;
EOT
        );
    }

    /**
     * Retrieve the zf-specific metadata from the "extra" section
     *
     * @param array $extra
     * @return array
     */
    private function getExtraMetadata(array $extra)
    {
        return isset($extra[self::EXTRA_KEY]) && is_array($extra[self::EXTRA_KEY])
            ? $extra[self::EXTRA_KEY]
            : [];
    }

    /**
     * @param PackageEvent $event
     * @return void
     */
    public function onPrePackageUninstall(PackageEvent $event)
    {
        $package = $event->getOperation()->getPackage();
        $name    = $package->getName();
        $extra   = $this->getExtraMetadata($package->getExtra());

        if (empty($extra)) {
            // Package does not define anything of interest; do nothing.
            return;
        }

        $prependBackslashCallback = [
            $this,
            'prependBackslash'
        ];
        $configFile               = ($this->projectRoot ? $this->projectRoot . '/' : '') . 'config/config.php';
        $config                   = is_file($configFile) ? include $configFile : [];
        $config                   = $config ?: [];
        $configItems              =
            array_diff(
                array_map(
                    $prependBackslashCallback,
                    $config
                ),
                array_map(
                    $prependBackslashCallback,
                    $extra['config-provider']
                )
            );
        //$configVar = var_export($config, true);
        $createClassConstantLiteralCallback = [
            $this,
            'createClassConstantLiteral'
        ];
        $config                             =
            array_map(
                $createClassConstantLiteralCallback,
                $configItems
            );
        $configItems                        = [];
        foreach ($config as $key => $configProvider) {
            $configItems[] = $key . ' => ' . $configProvider . ',';
        }
        $configItems = implode("\n", $configItems);
        $this->io->write(sprintf(
            "<info>    Removed the config provider(s):</info>",
            $name
        ));
        $this->io->write(sprintf(
            "<info>    " . implode(', ', $extra['config-provider']) . "</info>",
            $name
        ));
        $this->io->write(sprintf(
            "<info>    of %s</info>",
            $name
        ));
        file_put_contents(
            $configFile,
            <<< EOT
<?php
return
[
$configItems
]
;
EOT
        );
    }

    /**
     * @param $element
     * @return string|null
     */
    function prependBackslash($element): ?string
    {
        if (!is_string($element)) {
            return null;
        }

        if ($element[0] !== '\\') {
            $element = '\\' . $element;
        }

        return $element;
    }

    /**
     * @param $element
     * @return string|null
     */
    function createClassConstantLiteral($element): ?string
    {
        if (!is_string($element)) {
            return null;
        }

        if ($element[0] !== '\\') {
            $element = '\\' . $element;
        }

        return $element . '::class';
    }
}
